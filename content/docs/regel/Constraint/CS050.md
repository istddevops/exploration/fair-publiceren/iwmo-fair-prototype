---
regel:
  type: Constraint
  code: CS050
---
## Documentatie 

Als Partnernaam gevuld is, wordt in NaamGebruik aangegeven hoe de persoon zijn naam wenst te gebruiken. Hiervoor kunnen de volgende waarden gebruikt worden:
1 (eigen naam), 2 (naam echtgenoot of geregistreerd partner of alternatieve naam), 3 (naam echtgenoot of geregistreerd partner gevolgd door eigen naam) of 4 (eigen naam gevolgd door naam echtgenoot of geregistreerd partner). Indien geen Partnernaam gevuld is, wordt verplicht 1 (eigen naam) gevuld.



** Generereerd door `create_content.py` op 28 February, 2022**


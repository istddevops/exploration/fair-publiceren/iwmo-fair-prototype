---
regel:
  type: Bedrijfsregel
  code: OP076
---
## Documentatie 

Gegevens over een contactpersoon mogen alleen worden opgenomen indien noodzakelijk voor communicatie met de client.



** Generereerd door `create_content.py` op 28 February, 2022**


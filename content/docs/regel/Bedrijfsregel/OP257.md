---
regel:
  type: Bedrijfsregel
  code: OP257
---
## Documentatie 

Bij een wijziging van de zorgvraag wordt voor het betreffende product een nieuwe toewijzing afgegeven of voor het betreffende product wordt een gewijzigde toewijzing gestuurd, zie ook OP033x1. 
Een eventueel bestaande toewijzing wordt beeindigd indien:  
* De omvang van de te leveren ondersteuning wijzigt: er is meer of minder ondersteuning nodig op hetzelfde product dan vastgesteld in de bestaande toewijzing, behalve wanneer de omvang is gedefinieerd met een frequentie totaal binnen geldigheidsduur. 
* Het product wijzigt: de client heeft recht op een ander product dan vastgesteld in de bestaande toewijzing.

Een eventueel bestaande toewijzing wordt aangepast indien:  
* De einddatum van de te leveren zorg of ondersteuning wijzigt (intrekken of oprekken) 
* Het volume wijzigt bij een frequentie  totaal binnen geldigheidsduur toewijzing 
* Het maximaal budget wordt gewijzigd
Indien de gemeente het initiatief neemt tot aanpassing van het volume of het maximale budget , mag dit alleen in overleg met en na instemming van de aanbieder worden gedaan.



** Generereerd door `create_content.py` op 28 February, 2022**


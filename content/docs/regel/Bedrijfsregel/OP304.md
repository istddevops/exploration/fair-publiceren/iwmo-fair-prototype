---
regel:
  type: Bedrijfsregel
  code: OP304
---
## Documentatie 

Een declaratie-antwoordbericht bevat alleen detailinformatie over clienten waarvan berichtklassen zijn afgekeurd Clienten waarvan alle berichtklassen volledig zijn goedgekeurd worden dus niet mee teruggestuurd in het declaratie-antwoordbericht. Het declaratie-antwoordbericht bevat alleen clienten waarvan in 1 of meer Prestaties fouten zijn geconstateerd. In dat geval wordt de berichtklasse Client inclusief alle afgekeurde Prestaties retour gezonden, voorzien van de bijbehorende retourcodes.



** Generereerd door `create_content.py` op 28 February, 2022**


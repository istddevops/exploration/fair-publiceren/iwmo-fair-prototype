---
regel:
  type: Bedrijfsregel
  code: OP347
---
## Documentatie 

Stapeling betekent dat voor een client hetzelfde zorg- of ondersteuningsproduct meerdere keren wordt aangevraagd door dezelfde aanbieder voor (gedeeltelijk) dezelfde periode.    
Hetzelfde zorg- of ondersteuningsproduct betekent dezelfde productcode of dezelfde productcategorie indien productcode niet gevuld is. Indien productcategorie leeg is, geldt dat er geen ander aangevraagd product voor (gedeeltelijk) dezelfde periode naast mag staan.   
De producten uit de hulpmiddelencategorieen (categorie 11, 12, 13, 14) zijn hiervan uitgezonderd. 
Gecorrigeerde toewijzingen (waarvan de einddatum gelijk is aan de begindatum en de reden wijziging gelijk is aan 01), maken geen onderdeel uit van de bepaling of er sprake is van stapeling en worden niet opgenomen in de aanvraag. Deze toewijzingen worden niet gezien als actuele toewijzing. 




** Generereerd door `create_content.py` op 28 February, 2022**


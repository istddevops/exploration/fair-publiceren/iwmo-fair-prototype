---
regel:
  type: Bedrijfsregel
  code: OP266
---
## Documentatie 

Indien de gemeente een toewijzingbericht verzonden heeft, declareert de aanbieder hetzelfde of een nader gespecificeerd zorg- of ondersteuningsproduct. Dit betekent dat de declaratie of factuur hetzelfde óf (een) nader gespecificeerd(e) zorg- of ondersteuningsproduct(en) bevat als de toewijzing.  

Bij een specifieke toewijzing (productcategorie en productcode zijn beiden gevuld in de toewijzing) wordt hetzelfde zorg- of ondersteuningsproduct gedeclareerd of gefactuureerd, ofwel dezelfde combinatie van productcategorie en productcode. 
Bij een aspecifieke toewijzing (in de toewijzing is alleen productcategorie gevuld; productcode is leeg) worden één of meer nader gespecificeerde zorg- of ondersteuningsproducten gedeclareerd of gefactureerd. Dat betekent dat de aanbieder binnen de toegewezen productcategorie één of meer productcode(s) declareert of factureert die volgens de gehanteerde productcodelijst horen bij die productcategorie.



** Generereerd door `create_content.py` op 28 February, 2022**


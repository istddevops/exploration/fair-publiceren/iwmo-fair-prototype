---
regel:
  type: Bedrijfsregel
  code: OP344
---
## Documentatie 

Een verzoek om toewijzing of verzoek om wijziging bevat 1 of meerdere toe te wijzen of te wijzigen producten. 
De gemeente reageert hierop met een of meer toegewezen producten in een toewijzingbericht, 
of geeft met een antwoordbericht te kennen dat het verzoek wordt afgewezen.
Eventueel is er eerst onderzoek nodig voordat wordt toegewezen of afgewezen.
Bij een verzoek om wijziging is de reactie van de gemeente altijd op het niveau van het complete verzoek,
bij een verzoek om toewijzing kan de gemeente besluiten om elk aangevraagd product apart al dan niet te honoreren.



** Generereerd door `create_content.py` op 28 February, 2022**


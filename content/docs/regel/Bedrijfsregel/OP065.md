---
regel:
  type: Bedrijfsregel
  code: OP065
---
## Documentatie 

Clienten waarvan alle berichtklassen volledig zijn goedgekeurd worden dus niet mee teruggestuurd in het retourbericht. Het retourbericht bevat alleen clienten waarvan in één of meer berichtklassen, over of behorend bij die client, fouten zijn geconstateerd of waarvan, in het geval van het declaratie-/factuurbericht, het ingediende bedrag niet (volledig) wordt toegekend. In dat geval wordt de berichtklasse Client inclusief alle onderliggende berichtklassen retour gezonden, voorzien van retourcodes.



** Generereerd door `create_content.py` op 28 February, 2022**


---
regel:
  type: Bedrijfsregel
  code: OP263
---
## Documentatie 

Wanneer gemeenten en aanbieders kiezen voor een outputgerichte werkwijze moeten er duidelijke afspraken worden gemaakt over wat precies wordt verstaan onder de gewenste output en hoe kan worden vastgesteld hoe de output is behaald.



** Generereerd door `create_content.py` op 28 February, 2022**


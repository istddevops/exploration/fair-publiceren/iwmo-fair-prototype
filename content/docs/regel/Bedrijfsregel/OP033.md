---
regel:
  type: Bedrijfsregel
  code: OP033
---
## Documentatie 

Correcties op een melding start product zijn uitsluitend toegestaan voor het geleverde product. Als de ondersteuning beeindigd is, kan de aanvang niet meer op deze manier gecorrigeerd worden.

Met de status aanlevering van een berichtklasse kan worden aangegeven of:
een berichtklasse nieuw is (waarde 1);
een berichtklasse gewijzigd is (waarde 2);
een berichtklasse verwijderd moet worden (waarde 3). Een verwijdering betekent dat de vorige aanlevering met dezelfde sleutel als niet verzonden beschouwd moet worden.



** Generereerd door `create_content.py` op 28 February, 2022**


---
regel:
  type: Technische regel
  code: TR063
---
## Documentatie 

Gezien het corrigerende karakter van een aanlevering met StatusAanlevering 3, is het niet nodig om hierop overige inhoudelijke controles uit te voeren.

Opmerking:
Deze regel is bedoeld om het gebruik van de waarde 3 in goede banen te leiden en zegt niets over het mogelijk toegestaan zijn van deze waarde. Eventuele beperkingen van dit gebruik worden via een constraint (of eventueel andere technische regels) beschreven.



** Generereerd door `create_content.py` op 28 February, 2022**


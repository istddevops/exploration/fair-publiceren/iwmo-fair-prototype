---
regel:
  type: Technische regel
  code: TR373
---
## Documentatie 

Als in een verzoek om wijziging een budgetaanpassing wordt gevraagd, is dit altijd voor de gehele looptijd van de betreffende toewijzing.



** Generereerd door `create_content.py` op 28 February, 2022**


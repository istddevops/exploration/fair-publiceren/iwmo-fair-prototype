---
regel:
  type: Technische regel
  code: TR339
---
## Documentatie 

Indien de ProductCategorie in de Toewijzing gevuld is, dan moet in het declaratiebericht in de Prestatie de toegewezen ProductCategorie overgenomen worden.  




** Generereerd door `create_content.py` op 28 February, 2022**


---
regel:
  type: Technische regel
  code: TR305
---
## Documentatie 

ProductCategorie is in de Toewijzing altijd verplicht gevuld. In het declaratie- of factuurbericht moet in de Prestatie altijd de toegewezen ProductCategorie overgenomen worden.



** Generereerd door `create_content.py` op 28 February, 2022**


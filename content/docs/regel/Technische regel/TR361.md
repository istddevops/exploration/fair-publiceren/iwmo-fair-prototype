---
regel:
  type: Technische regel
  code: TR361
---
## Documentatie 

Voor producten die zijn opgenomen in de berichtklasse TeWijzigenProduct of OngewijzigdProduct moet op basis van het ToewijzingNummer de overlap op de aangevraagde producten gecontroleerd worden.Indien de productcode niet is meegegeven dan geldt deze technische regel op niveau van productcategorie. Indien productcategorie leeg is, dan mag dit met geen enkel ander aangevraagd product overlappen.

Echter voor Producten uit ProductCategorie 11, 12, 13 en 14 mogen zorgperiodes overlappen, ook op niveau van categorie. Indien ProductCategorie leeg is, mag deze toewijzing alleen overlappen met toewijzingen met categorie 11, 12, 13 en 14.



** Generereerd door `create_content.py` op 28 February, 2022**


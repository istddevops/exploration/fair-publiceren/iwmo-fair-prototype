---
regel:
  type: Invulinstructie
  code: IV073
---
## Documentatie 

De soort zorg (product) bepaalt of een aanbieder btw moet afdragen. Als een aanbieder zorg verleent waarover btw moet worden afgedragen, dan is hij btw plichtig. 

Wat is verschil tussen een factuur en een declaratie m.b.t. btw?
BtwIDNummer moet altijd gevuld worden als de hulpverlener btw-plichtig is. Per ingediende prestatie (dus per gedeclareerde/gefactureerde product) wordt aangegeven of de aanbieder voor dat product btw moet afdragen (BtwVrijstellingsIndicatie).

Factuur
Indien code bericht gevuld is met waarde 448 of 450 (= Factuur Wmo-ondersteuning of Factuur Jeugdhulp), dan moet indicatie BTW-vrijstelling voorkomen. (zie CD059)
Indien BtwVrijstellingIndicatie gevuld is met waarde 1 (Btw-vrijstelling), dan mogen BtwPercentage en BtwBedrag niet voorkomen. (zie CD056)
Indien BtwVrijstellingIndicatie gevuld is met waarde 2 (Geen Btw-vrijstelling), dan moeten BtwIDNummer, BtwTotaalBedrag, BtwPercentage en BtwBedrag voorkomen (zie CD055)
BtwIDNummer moet gevuld worden als de hulpverlener btw-plichtig is, ongeacht of er in het bestand wel of niet sprake is van gedeclareerde/gefactureerde hulp waar btw op van toepassing is.

Declaratie
In het geval van een declaratie zijn alle bedragen inclusief btw als over dat product btw afgedragen moet worden. 



** Generereerd door `create_content.py` op 28 February, 2022**


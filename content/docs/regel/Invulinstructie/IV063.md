---
regel:
  type: Invulinstructie
  code: IV063
---
## Documentatie 

Voor een product volgt precies één Prestatie per declaratie-/factuurperiode, ongeacht het aantal hulpperiodes dat binnen de declaratie-/factuurperiode ligt.

De ProductPeriode Begindatum wordt gevuld met:
de DeclaratiePeriode Begindatum van de declaratie-/factuurperiode indien de Ingangsdatum van het ToegewezenProduct voor of op de DeclaratiePeriode Begindatum ligt.
de Ingangsdatum van het ToegewezenProduct indien de Ingangsdatum ToegewezenProduct na de DeclaratiePeriode Begindatum ligt.

De ProductPeriode Einddatum wordt gevuld met:
de DeclaratiePeriode Einddatum van de declaratie-/factuurperiode indien de Einddatum van het ToegewezenProduct op of na de DeclaratiePeriode Einddatum ligt.
de Einddatum van het ToegewezenProduct indien de Einddatum ToegewezenProduct voor de DeclaratiePeriode Einddatum ligt.

Facturatie/Declaratie systematiek
Gemeenten en aanbieders kunnen (op grond van OP264) onderling afwijkende afspraken maken over de momenten waarop, al dan niet in delen, gedeclareerd of gefactureerd kan worden, in de situatie van een outputgericht product dat in euro's is toegewezen.
Als het resultaat van een hulpperiode voor een outputgericht product pas aan het eind van die hulpperiode kan worden vastgesteld, wordt de productperiode gelijk gesteld aan de declaratie-/factuurperiode waarin het resultaat is vastgesteld tenzij de gemeente en aanbieder afwijkende afspraken hebben gemaakt over het aantal declaratie momenten. Hierbij wordt de ProductPeriode indien nodig aangepast zodat de productperiode ook in deze situatie altijd binnen de periode van de toewijzing valt.

Voorbeelden
In onderstaande voorbeelden wordt uitgegaan van de volgende situatie:
Er wordt per maand gedeclareerd/gefactureerd
Het ToegewezenProduct heeft Ingangsdatum 11-04-2019 en Einddatum 18-06-2019 (dit is de toegewezen periode)
In de voorbeelden staan de ProductPeriodes in de declaratie/factuur voor de maanden April, Mei en Juni benoemd

1: Ononderbroken hulpperiode
Client ontvangt vanaf het begin t/m het eind van de toegewezen periode ononderbroken hulp. Er volgt per maand één Prestatie met de volgende ProductPeriodes:
April: 11-04-2019 t/m 30-04-2019
Mei: 01-05-2019 t/m 31-05-2019
Juni: 01-06-2019 t/m 18-06-2019

2: Onderbroken hulpperiode
Client ontvangt onderbroken hulp in de volgende periodes:
- 12-04-2019 t/m 15-04-2019
- 23-04-2019 t/m 08-05-2019
- 13-05-2019 t/m 20-05-2019
- 03-06-2019 t/m 07-06-2019
- 11-06-2019 t/m 17-06-2019
Er volgt per maand één Prestatie met de volgende ProductPeriodes:
April: 11-04-2019 t/m 30-04-2019
Mei: 01-05-2019 t/m 31-05-2019
Juni: 01-06-2019 t/m 18-06-2019

3: Outputgericht met resultaat op eind van hulpperiode
Client ontvangt onderbroken of ononderbroken outputgerichte hulp vanaf het begin tot het eind van toegewezen periode (11-04-2019 t/m 18-06-2019). Pas op 18-06-2019 kan het resultaat van de outputgerichte hulp worden vastgesteld. In de maanden April en Mei wordt er geen declaratie/factuur ingediend voor deze client. Er volgt in de maand juni één Prestatie met de volgende ProductPeriode:
Juni: 01-06-2019 t/m 18-06-2019



** Generereerd door `create_content.py` op 28 February, 2022**


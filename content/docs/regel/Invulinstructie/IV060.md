---
regel:
  type: Invulinstructie
  code: IV060
---
## Documentatie 

Indien gemeente en aanbieder een tariefeenheid hebben afgesproken die afwijkt van de Eenheid in de Prestatie, dan moet in de Prestatie het tarief per Eenheid worden omgerekend en afgerond op 2 decimalen. De regels hiervoor staan weergegeven in onderstaande tabel. De afronding vindt plaats op het niveau van Eenheid in de Prestatie. Dus indien een tarief per uur is afgesproken, maar per minuut moet worden gedeclareerd, wordt als eerste het tarief per minuut berekend en afgerond volgens de regels die hier staan beschreven. Dit (afgeronde) tarief wordt in de Prestatie opgenomen en gebruikt om BerekendBedrag te vullen.
Afronden gebeurt op basis van de 3e positie achter de komma. Dit betekent dat 1,455 wordt afgerond naar 1,46 en dat 1,454 wordt afgerond naar 1,45.



** Generereerd door `create_content.py` op 28 February, 2022**


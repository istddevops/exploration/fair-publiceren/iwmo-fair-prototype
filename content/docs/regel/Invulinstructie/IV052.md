---
regel:
  type: Invulinstructie
  code: IV052
---
## Documentatie 

De aanbieder dient het beschikkingnummer mee te geven in het Verzoek om Toewijzing bericht indien de client van haar gemeente een beschikking heeft gekregen. 

De gemeente kan op basis van het beschikkingnummmer eenvoudig de bestaande beschikking koppelen aan de informatie uit het Verzoek Om Toewijzing bericht.



** Generereerd door `create_content.py` op 28 February, 2022**


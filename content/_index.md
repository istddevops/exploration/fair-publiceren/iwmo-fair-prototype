---
title: Introductie
description: >
  Prototype FAIR Publiceren op basis van iWmo 3.1 voorbeelden.
bookToC: true
---

| Versie | Datum | Auteur(s) | Omschrijving |
|:-|:-|:-|:-|
| 0.0.1 | 15-02-2022 | Onno Haldar | Eerste versie op basis van gegenereerde modellen (vanuit automatisch geconverteerde Regelrapport Excel) in `PlantUML` en relaties nog handmatig voorbeeld Bericht WMO305.<br> Informele evaluatie met: Roberto, Elly en Mathijs gedaan.|

## Informatiemodel Wmo

{{< kroki name="wmo-informatiemodel-intro" >}}
```plantuml
@startuml prototype
skinparam handwritten true
!$berichtenURL = "<%= docsURL %>/bericht"

package "Metamodel" {

  package "Processen" as Processen #APPLICATION {

  }

  package "Regels" as Regels #APPLICATION {

  }

  package "Berichten" as Berichten #APPLICATION {

    rectangle "WMO305" as WMO305 #CornFlowerBlue {

    }

    url of WMO305 is [[$berichtenURL/wmo305]]
  }
  url of Berichten is [[$berichtenURL]]

  package "Gegevens" as Gegevens #APPLICATION {

  }

  Processen -[hidden]-> Regels
  Regels -[hidden]-> Berichten
  Berichten -[hidden]-> Gegevens
}

@enduml
```
{{< /kroki >}}



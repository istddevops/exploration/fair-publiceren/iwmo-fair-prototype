# local imports
from istd_klassen import *
from export_data import *
from import_regelrapport import *
from create_content import *

# Converteer Regelrapport naar Go Hugo Contanr en Data.
regelRapportFilePath = 'iwmo-bronnen/Release 3.0.4/excels/regelrapport-iwmo-3.0.4.xlsx'
berichtenElemRegels = importRegelsPerBerichtelement(regelRapportFilePath)
berichten = berichtenElemRegels['berichten']
dataTypen = berichtenElemRegels['dataTypen']
regels = importRegels(regelRapportFilePath)

# Exporteer geconverteerde Data
exportBerichtenYamls(berichten)
exportDataTypenYamls(dataTypen)
exportRegelYamls(regels)

# Maak Site Content vanuit geconverteerde data
createBerichtenKlassenContent(berichten)
createDataTypenContent(dataTypen)
createRegelsContent(regels)
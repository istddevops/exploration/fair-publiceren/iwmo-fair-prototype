""" Functions to export YAML-data """

# package imports
from io import TextIOWrapper
import os

# local imports
from istd_klassen import *

def defaultDataPath(rootPath: str = ''):
    """Standaard HUGO SSG Data Locatie."""
    return os.path.join(rootPath, 'data')

# return string between single quotes
def txtValue(inputStr: str):
    if inputStr != None:
        return inputStr
    else:
        return ""

def printBeschrijving(dataFile: TextIOWrapper, beschrijving: str = [], dataIndent: str = ''):
    lineIdent = dataIndent + '  '
    print(dataIndent + 'beschrijving: |', file=dataFile)
    if len(beschrijving) > 0:
        # 1 of meerdere documentatie regels gevonden
        for docLine in beschrijving:
            print(lineIdent + docLine, file=dataFile)
    else:
        # geen beschrijving gevonden
        print(lineIdent + '(geen beschrijving gevonden)', file=dataFile)

def exportRegelYamls(regels: IstdRegel = [], exportPath: str = defaultDataPath()):
    """Exporteer regels in YAML-formaat."""
    # initialisatie
    regelPath = os.path.join(exportPath, 'regel')
    os.makedirs(regelPath, exist_ok=True)
    # schrijf YAML-bestand
    for regel in regels:
        # schrijf regel directory "type" / bestand "code"
        regelTypePath = os.path.join(regelPath, regel.type)
        os.makedirs(regelTypePath, exist_ok=True)
        regelFilePath = os.path.join(regelTypePath, regel.code + '.yml')
        with open(regelFilePath, 'w') as regelDataFile:
            # verplichte waarden
            print('type: ' + regel.type, file=regelDataFile)
            print('code: ' + regel.code, file=regelDataFile)
            print('titel: |\n  ' + txtValue(regel.titel.split(regel.code + ': ')[1]), file=regelDataFile)
            # optionele waarden
            if regel.XSDRestrictie != None:
                print('XSDRestrictie: ' + regel.XSDRestrictie, file=regelDataFile)
            if regel.retourcode != None:
                print('retourcode: ' , file=regelDataFile)
                retourcodeLijst = regel.retourcode.split(',')
                for retourcodeItem in retourcodeLijst:
                    print('- ' + retourcodeItem.strip(), file=regelDataFile)
            if regel.controleniveau != None:
                print('controleniveau: ' + regel.controleniveau, file=regelDataFile)
            if regel.referentieregel != None:
                print('referentieregel: ' + regel.referentieregel, file=regelDataFile)

def exportDataTypenYamls(dataTypen: IstdDataType = [], exportPath: str = defaultDataPath()):
    """Exporteer datatypen in YAML-formaat."""
    # initialisatie
    dataTypePath = os.path.join(exportPath, 'datatype')
    os.makedirs(dataTypePath, exist_ok=True)
    # schrijf YAML-bestand
    for dataType in dataTypen:
        dataTypeFilePath = os.path.join(dataTypePath, dataType.naam + '.yml')
        with open(dataTypeFilePath, 'w') as dataTypeDataFile:
            # body
            print('type: datatype', file=dataTypeDataFile)
            print('naam: ' + dataType.naam, file=dataTypeDataFile)
            printBeschrijving(dataTypeDataFile, dataType.beschrijving)
            # codelijst
            if dataType.codeLijst != None:
                print('codeLijst: ' + dataType.codeLijst, file=dataTypeDataFile)
            # regels
            if len(dataType.regels) > 0:
                print('regel:', file=dataTypeDataFile)
                for regel in dataType.regels:
                    print('- ' + regel, file=dataTypeDataFile)
            # elementen
            if len(dataType.elementen) > 0:
                print('element:', file=dataTypeDataFile)
                for element in dataType.elementen:
                    print('- naam: ' + element.naam, file=dataTypeDataFile)
                    printBeschrijving(dataTypeDataFile, element.beschrijving, '  ')
                    if element.dataType != None:
                        print('  dataType: ' + element.dataType, file=dataTypeDataFile)
                    # element-regels
                    if len(element.regels) > 0:
                        print('regel:', file=dataTypeDataFile)
                        for regel in element.regels:
                            print('- ' + regel, file=dataTypeDataFile)


def exportKlasseYaml(klasse: IstdKlasse, berichtNaam: str, exportPath: str = defaultDataPath()):
    """Exporteer Klasse in YAML-formaat."""
    # initialisatie
    klasseDataPath = os.path.join(exportPath, 'klasse', klasse.naam)
    os.makedirs(klasseDataPath, exist_ok=True)
    klasseDataFilePath = os.path.join(klasseDataPath, berichtNaam  + '.yml')
    # schrijf YAML-bestand
    with open(klasseDataFilePath, 'w') as klasseDataFile:
        # body
        print('type: ' + klasse.naam, file=klasseDataFile)
        print('naam: ' + berichtNaam, file=klasseDataFile)
        printBeschrijving(klasseDataFile, klasse.beschrijving)
        # regels
        if len(klasse.regels) > 0:
            print('regel:', file=klasseDataFile)
            for regel in klasse.regels:
                print('- ' + regel, file=klasseDataFile)
        # elementen
        print('element:', file=klasseDataFile)
        for element in klasse.elementen:
            print('- naam: ' + element.naam, file=klasseDataFile)
            # beschrijving
            printBeschrijving(klasseDataFile, element.beschrijving, '  ')
            # Data Type
            print('  dataType: ' + element.dataType, file=klasseDataFile)
            # element-sleutel
            if element.sleutel:
                print('  sleutel: True', file=klasseDataFile)
            # element-regels
            if len(element.regels) > 0:
                print('  regel:', file=klasseDataFile)
                for regel in element.regels:
                    print('  - ' + regel, file=klasseDataFile)

    
def exportBerichtYaml(bericht: IstdBericht, berichtDataPath: str = None, exportPath: str = defaultDataPath()):
    """Exporteer Bericht in YAML-formaat."""
    # initialisatie
    berichtDataFilePath = os.path.join(berichtDataPath, bericht.naam + '.yml')
    header = bericht.naam
    # schrijf YAML-bestand
    with open(berichtDataFilePath, 'w') as berichtDataFile:
        # Body
        print('type: bericht', file=berichtDataFile)
        print('naam: ' + bericht.naam, file=berichtDataFile)
        printBeschrijving(berichtDataFile, bericht.beschrijving)
        # Regels
        if len(bericht.regels) > 0:
            print('regel:', file=berichtDataFile)
            for regel in bericht.regels:
                print('- ' + regel, file=berichtDataFile)
        # Klassen
        print('klasse:', file=berichtDataFile)
        for klasse in bericht.klassen:
            print('- ' + klasse.naam, file=berichtDataFile)
            exportKlasseYaml(klasse, bericht.naam, exportPath)
        # Relaties
        print('relatie:', file=berichtDataFile)
        for relatie in bericht.relaties:
            print('- naam: ' + relatie.naam, file=berichtDataFile)
            if relatie.parentKlasse == None: print(bericht.naam + '==> geen parentklasse voor relatie ' + relatie.naam)
            print('  parentKlasse: ' + str(relatie.parentKlasse), file=berichtDataFile)
            if relatie.childKlasse == None: print(bericht.naam + '==> geen childklasse voor relatie ' + relatie.naam)
            print('  childKlasse: ' + str(relatie.childKlasse), file=berichtDataFile)
            print('  childMin: ' + relatie.childMin, file=berichtDataFile)
            print('  childMax: ' + relatie.childMax, file=berichtDataFile)

def exportBerichtenYamls(berichten: IstdBericht = [], exportPath: str = defaultDataPath()):
    """Exporteer Berichten in YAML-formaat."""
    # Exporteer Berichten (en onderliggende Klassen)
    berichtDataPath = os.path.join(exportPath, 'bericht')
    os.makedirs(berichtDataPath, exist_ok=True)
    for bericht in berichten:
        exportBerichtYaml(bericht, berichtDataPath, exportPath)

""" Functions to export YAML-data """

from datetime import date

# package imports
import os

# local imports
from istd_klassen import *

def generatieStempel():
    stempel = '\n** Generereerd door `'
    stempel = stempel + os.path.basename(__file__)
    stempel = stempel + '` op ' + date.today().strftime("%d %B, %Y") + '**\n'
    return  stempel

def defaultContentDocsPath(contentPath: str = 'content'):
    """Standaard HUGO SSG Content Locatie."""
    return os.path.join(contentPath, 'docs')

def createRegelsContent(regels: IstdRegel = [], contentPath: str = defaultContentDocsPath()):
    """Maak Content voor Regels in Markdown-formaat."""
    # initialisatie
    regelContentPath = os.path.join(contentPath, 'regel')
    os.makedirs(regelContentPath, exist_ok=True)
    # Regel index-MD
    regelContentIndexFilePath = os.path.join(regelContentPath, '_index.md')
    with open(regelContentIndexFilePath, 'w') as regelContentIndexFile:
        # body
        print('---', file=regelContentIndexFile)
        print('bookCollapseSection: true', file=regelContentIndexFile)
        print('title: Regels', file=regelContentIndexFile)
        print('description: PM', file=regelContentIndexFile)
        print('---', file=regelContentIndexFile)
        print(generatieStempel(), file=regelContentIndexFile)
        print('{{< sectiontable >}}', file=regelContentIndexFile)
    # Type Regel MD-bestanden
    for regel in regels:
        regelTypeContentPath = os.path.join(regelContentPath, regel.type)
        if not os.path.exists(regelTypeContentPath):
            os.makedirs(regelTypeContentPath, exist_ok=False)
            # Regel Type index-bestand
            regelTypeContentIndexFilePath = os.path.join(regelTypeContentPath, '_index.md')
            with open(regelTypeContentIndexFilePath, 'w') as regelTypeContentIndexFile:
                # body
                print('---', file=regelTypeContentIndexFile)
                print('bookCollapseSection: true', file=regelTypeContentIndexFile)
                print('title: ' + regel.type, file=regelTypeContentIndexFile)
                print('description: PM', file=regelTypeContentIndexFile)
                print('---', file=regelTypeContentIndexFile)
                print(generatieStempel(), file=regelTypeContentIndexFile)
                print('{{< sectiontable >}}', file=regelTypeContentIndexFile)
        # Regel MD-bestand
        regelContentFilePath = os.path.join(regelTypeContentPath, regel.code + '.md')
        with open(regelContentFilePath, 'w') as regelContentFile:
            print('---', file=regelContentFile)
            print('regel:', file=regelContentFile)
            print('  type: ' + regel.type, file=regelContentFile)
            print('  code: ' + regel.code, file=regelContentFile)
            print('---', file=regelContentFile)
            if regel.documentatie != None:
                print('## Documentatie \n\n' + regel.documentatie + '\n\n', file=regelContentFile)
            print(generatieStempel(), file=regelContentFile)

def createDataTypenContent(dataTypen: IstdDataType = [], contentPath: str = defaultContentDocsPath()):
    """Maak Content voor Datatypen in Markdown-formaat."""
    # initialisatie
    dataTypeContentPath = os.path.join(contentPath, 'datatype')
    os.makedirs(dataTypeContentPath, exist_ok=True)
    # index-bestand
    dataTypeContentIndexFilePath = os.path.join(dataTypeContentPath, '_index.md')
    with open(dataTypeContentIndexFilePath, 'w') as dataTypeContentIndexFile:
        # body
        print('---', file=dataTypeContentIndexFile)
        print('bookCollapseSection: true', file=dataTypeContentIndexFile)
        print('title: Datatypen', file=dataTypeContentIndexFile)
        print('description: PM', file=dataTypeContentIndexFile)
        print('---', file=dataTypeContentIndexFile)
        print(generatieStempel(), file=dataTypeContentIndexFile)
        print('{{< sectiontable >}}', file=dataTypeContentIndexFile)
    # schrijf Md-bestanden
    for dataType in dataTypen:
        dataTypeContentFilePath = os.path.join(dataTypeContentPath, dataType.naam + '.md')
        with open(dataTypeContentFilePath, 'w') as contentTypeDataFile:
            # body
            print('---', file=contentTypeDataFile)
            print('dataType: ' + dataType.naam, file=contentTypeDataFile)
            print('---', file=contentTypeDataFile)
            print(generatieStempel(), file=contentTypeDataFile)

def createKlasseContent(klasse: IstdKlasse, berichtNaam: str, contentPath: str = defaultContentDocsPath()):
    """Maak Content voor Klassen in Markdown-formaat."""
    # initialisatie
    klasseContentPath = os.path.join(contentPath, 'klasse', klasse.naam)
    os.makedirs(klasseContentPath, exist_ok=True)
    # klasse index-bestand
    klasseContentIndexFilePath = os.path.join(klasseContentPath, '_index.md')
    with open(klasseContentIndexFilePath, 'w') as klasseContentIndexFile:
        print('---', file=klasseContentIndexFile)
        print('bookCollapseSection: true', file=klasseContentIndexFile)
        print('title: ' + klasse.naam, file=klasseContentIndexFile)
        print('description: Klassegroep', file=klasseContentIndexFile)
        print('---', file=klasseContentIndexFile)
        print(generatieStempel(), file=klasseContentIndexFile)
        print('{{< sectiontable >}}', file=klasseContentIndexFile)

    # schrijf Klasse-bestand
    klasseContentFilePath = os.path.join(klasseContentPath, berichtNaam  + '.md')
    with open(klasseContentFilePath, 'w') as klasseContentFile:
        # body
        print('---', file=klasseContentFile)
        print('klasse:', file=klasseContentFile)
        print('  type: ' + klasse.naam, file=klasseContentFile)
        print('  naam: ' + berichtNaam, file=klasseContentFile)
        print('---', file=klasseContentFile)
        print(generatieStempel(), file=klasseContentFile)
    
def createBerichtContent(bericht: IstdBericht, berichtContentPath: str = None, contentPath: str = defaultContentDocsPath()):
    """Maak Content voor Bericht en onderliggende Klassen  in Markdown-formaat."""
    # initialisatie
    berichtContentFilePath = os.path.join(berichtContentPath, bericht.naam + '.md')
    # schrijf YAML-bestand
    with open(berichtContentFilePath, 'w') as berichtContentFile:
        # body
        print('---', file=berichtContentFile)
        print('bericht: ' + bericht.naam, file=berichtContentFile)
        print('---', file=berichtContentFile)
        for klasse in bericht.klassen:
            createKlasseContent(klasse, bericht.naam, contentPath)

def createBerichtenKlassenContent(berichten: IstdBericht = [], contentPath: str = defaultContentDocsPath()):
    """Maak Content voor Berichten en onderliggende Klassen in Markdown-formaat."""
    # initialisatie
    berichtContentPath = os.path.join(contentPath, 'bericht')
    os.makedirs(berichtContentPath, exist_ok=True)
    klasseContentPath = os.path.join(contentPath, 'klasse')
    os.makedirs(klasseContentPath, exist_ok=True)
    # index-bestanden
    berichtContentIndexFilePath = os.path.join(berichtContentPath, '_index.md')
    with open(berichtContentIndexFilePath, 'w') as berichtContentIndexFile:
        # body
        print('---', file=berichtContentIndexFile)
        print('bookCollapseSection: true', file=berichtContentIndexFile)
        print('title: Berichten', file=berichtContentIndexFile)
        print('description: PM', file=berichtContentIndexFile)
        print('---', file=berichtContentIndexFile)
        print(generatieStempel(), file=berichtContentIndexFile)
        print('{{< sectiontable >}}', file=berichtContentIndexFile)

    klasseContentIndexFilePath = os.path.join(klasseContentPath, '_index.md')
    with open(klasseContentIndexFilePath, 'w') as klasseContentIndexFile:
        # body
        print('---', file=klasseContentIndexFile)
        print('bookCollapseSection: true', file=klasseContentIndexFile)
        print('title: Klassen', file=klasseContentIndexFile)
        print('description: PM', file=klasseContentIndexFile)
        print('---', file=klasseContentIndexFile)
        print(generatieStempel(), file=klasseContentIndexFile)
        print('{{< sectiontable >}}', file=klasseContentIndexFile)
    # bericht en klassen bestanden
    for bericht in berichten:
        createBerichtContent(bericht, berichtContentPath, contentPath)

from import_xsds import importBerichtXsd

# Interne Bouw test
testBericht = importBerichtXsd('iwmo-bronnen/Release 3.0.4/xsds/WMO305.xsd')
print('YAML-weergave Testbericht')
print('======================================================================================')
print('naam: ' + testBericht.naam)
print('beschrijving: ' + str(testBericht.beschrijving))
print('standaard: ' + testBericht.standaard)
print('release: ' + testBericht.release)
print('berichtXsdVersie: ' + testBericht.berichtXsdVersie)
print('berichtXsdMinVersie: ' + testBericht.berichtXsdMinVersie)
print('berichtXsdMaxVersie: ' + testBericht.berichtXsdMaxVersie)
print('basisschemaXsdVersie: ' + testBericht.basisschemaXsdVersie)
print('basisschemaXsdMinVersie: ' + testBericht.basisschemaXsdMinVersie)
print('basisschemaXsdMaxVersie: ' + testBericht.basisschemaXsdMaxVersie)
print('klasse:')
for klasse in testBericht.klassen:
    print('- naam: ' + klasse.naam)
    print('  beschrijving: ' + str(klasse.beschrijving))
    print('  element:')
    for element in klasse.elementen:
        print('  - naam: ' + element.naam)
        print('    beschrijving: ' + element.beschrijving)
        print('    dataType: ' + element.dataType)
        print('    waarde: ' + str(element.waarde))
print('relatie:')
for relatie in testBericht.relaties:
    print('- naam: ' + relatie.naam)
    print('  parentKlasse: ' + str(relatie.parentKlasse))
    print('  childKlasse: ' + str(relatie.childKlasse))
    print('  childMin: ' + relatie.childMin)
    print('  childMax: ' + relatie.childMax)
#
# Doel: iStandaard BizzDesign Output (Excels en XSDs) naar YMAL en Markdown Formaat Conversie om:
#       - Daarmee te komen tot een basis Data-Beheer waarmee diverse outputs kunnen worden gegenereerd.
#       - Uiteindelijk te komen tot uitfasering van de huidige BizzDesign (legacy) Applicatie
#
# Status: Verkenning op basis vanSSG HUGO (YAML-Data en Markdown-Content) output
#
# Auteur: A. Haldar (Onno)
#
# TO-DO 3-3-2022
# - Partials diagrammen bouwen en testen met CI/CD
# - Importeren basisschema.xsd => import_xsds => def importBasisSchemaXsd => return dataTypen
# - Denk aan => def importRegelsPerBerichtelement => return berichten, dataTypen!!!
# - Merge/Toevoegen Regels op Bericht, KLasse, Element en Datatype niveaus
# - Relaties tussen Regels via excel Roberto
# - Exports en Content maken voor demo

# Package imports
import os
import sys

# Local imports
from istd_klassen import *
from import_xsds import importBerichtXsd
from import_regelrapport import *
from export_data import *
from create_content import *

# Haal locatie van Bizz Output bestanden op
print(sys.argv)
importPath = sys.argv[1]
print('Importpath: ' + importPath)

# Initialisatie Conversie-Variabelen
converiseOutputPath = ''
#converiseOutputPath = 'test-output'
berichten: IstdBericht = []

# Importeer XSDs
importPathXsds = os.path.join(importPath, 'xsds')
print('importPathXsds: ' + importPathXsds)
for xsdFile in os.listdir(importPathXsds):
    xsdFileTst = xsdFile.lower()
    # Alleen XSD-bestanden verwerken
    if xsdFileTst.endswith('.xsd'):
        if not xsdFileTst == 'basisschema.xsd':
            print('Importeer Berichtschema: ' + xsdFile)
            berichten.append(importBerichtXsd(os.path.join(importPathXsds, xsdFile)))
            
        else :
            print('Importeer Basissschema')



# Importeer Excels
#importPathExcels = os.path.join(importPath, 'excels')
#for excelFile in os.listdir(importPathExcels):
#    if excelFile.lower().endswith('.xlsx'):
#        if excelFile.lower().startswith('regelrapport'):
#            print('Importeer Regelrapport')
#            regelRapportFilePath = os.path.join(importPath, 'excels', 'regelrapport.xlsx')
#            RegelRapportImport = importRegelsPerBerichtelement(regelRapportFilePath)
#            berichten = RegelRapportImport['berichten']
#            dataTypen = RegelRapportImport['dataTypen']
#            regels = importRegels(regelRapportFilePath)
#        elif excelFile.lower().startswith('codelijsten'):
#            print('Importeer Codelijsten')
#        else:
#            print('Onbekende Import-Excel ==> ' + excelFile)


# Exporteer YAML-Data
dataPath = defaultDataPath(converiseOutputPath)
print('dataPath = ' + dataPath)
exportBerichtenYamls(berichten, dataPath)
#exportDataTypenYamls(dataTypen)
#exportRegelYamls(regels)

# Maak Markdown-Content
#contentDocsPath = defaultContentDocsPath(os.path.join(converiseOutputPath, 'content'))
#createBerichtenKlassenContent(berichten)
#createDataTypenContent(dataTypen)
#createRegelsContent(regels)